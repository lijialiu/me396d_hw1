##########################################################################
# Implementation of paper "A Decision Framework for an Adaptive
#  Behavioral Intervention for Physical Activity Using Hybrid Model
#  Predictive Control"
# The behavior goal is 10000
##########################################################################
import picos as pic
import numpy as np
import matplotlib.pyplot as plt


def predictive_model(A, B, C, p):
        
        ###########################################
        #Y(k+1...k+p) = G * x(k) + H * U(k...K+p-1)
        ###########################################
        nC = np.size(C[:, 0])
        nB = np.size(B[0, :])
        nA = np.size(A[0, :])
        H = np.zeros(( nC * p, nB * p))
        P = C
        G = np.zeros(( nC * p, nA))

        for i in range(p):  
            H_temp = P.dot(B)
            for j in range(i, p):
                H[j * nC : (j + 1) * nC, (j - i) * nB : (j - i + 1) * nB] = H_temp
            P = P.dot(A)
            G[i*nC:(i+1)*nC] = P

        return G, H 

    
def cost_function( x_part, H, D, yr, ur, u9r, u, nu, ny, p):

        ######delta_Y_Yr############
        Qy = np.eye(p * ny)
        
        delta_Yr = ((H*u+x_part - yr).T) * Qy * (H*u + x_part - yr)

        #####delta_Uk_Uk+1###########
        delta_u = u.T * D_delta.T * D_delta * u

        #####delta_U_Ur###########
        delta_Ur = (u[0] - ur[0]) * (u[0] - ur[0])
        delta_Ur += (u[1] - u9r[0]) * (u[1] - u9r[0])
        for i in range(1, p): 
            delta_Ur += (u[i * nu] - ur[i]) * (u[i * nu] - ur[i])
            delta_Ur += (u[1 + i * nu] - u9r[i]) * (u[1 + i * nu] - u9r[i])
        
        return (delta_Yr + delta_u) + delta_Ur 
    
    
def difference_matrix_delta( nu, nh, penalty9):
        D = np.zeros((nu * nh, nu * nh))
        for i in range(nh - 1):
            temp = np.eye(nu)
            temp[1,1] = penalty9
            D[i * nu : (i+1) * nu, i * nu : (i+1) * nu] = temp
            D[i * nu : (i+1) * nu, (i+1) * nu : (i+2) * nu] = -temp
        return D

def difference_matrix( nu, nh):
    D = np.zeros((nu * nh, nu * nh))
    for i in range(nh - 1):
        temp = np.eye(nu)
        D[i * nu : (i+1) * nu, i * nu : (i+1) * nu] = temp
        D[i * nu : (i+1) * nu, (i+1) * nu : (i+2) * nu] = -temp
    return D



#####implementation of HMPC algorithm#############
def HMPC(u0, u1, u2, Gx, xk, nu, ny, nh, nu8, nu9, D, Pu, ur, u9r, add_constraint):


    prob = pic.Problem()
    
    
    u = prob.add_variable('u',(nh * nu,1), vtype='integer')
    d1 = prob.add_variable('d1',nh * len(v1), vtype='binary')
    z1 = prob.add_variable('z1',nh * len(v1), vtype='integer')
    d10 = prob.add_variable('d10',nh, vtype='binary')
    z10 = prob.add_variable('z10',nh, vtype='integer')
    
    D = pic.new_param('D',D)
    Pu = pic.new_param('Pu',Pu)
    
    x_part = Gx.dot(xk)
    x_part = pic.new_param('x_part',x_part)

    ###initial value constrains
    prob.add_constraint(u[0]==u0)
    prob.add_constraint(u[1]==u1)
    prob.add_constraint(u[2]==u2)
    
    if add_constraint == 1:
        prob.add_list_of_constraints( [u[1 + i * nu] == 0 for i in range(1,nh)], 'i','[1, nh]')


    # ##### u[1, 2, 3](k) <= [10000, 500, 500] and u[1, 2, 3](k) >= [5000, 0, 0]

    prob.add_list_of_constraints(
        [u[0 + i * nu] >= 5000 for i in range(0,nh)], 'i','[0, nh]'
            )
    prob.add_list_of_constraints(
        [u[1 + i * nu] >= 0 for i in range(0,nh)], 'i','[0, nh]'
            )
    prob.add_list_of_constraints(
        [u[2 + i * nu] >= 0 for i in range(0,nh)], 'i','[0, nh]'
            )
    prob.add_list_of_constraints(
        [u[0 + i * nu] <= 10000 for i in range(0,nh)], 'i','[0, nh]'
            )
    prob.add_list_of_constraints(
        [u[1 + i * nu] <= 500 for i in range(0,nh)], 'i','[0, nh]'
            )
    prob.add_list_of_constraints(
        [u[2 + i * nu] <= 500 for i in range(0,nh)], 'i','[0, nh]'
          )


    # ######detaU[1,2,3] <= [10000, 500, 500] and detaU[1,2,3] >= [-10000, -500, -500]
    prob.add_list_of_constraints(
        [(D*u)[0 + i * nu] >= -1000 for i in range(0,nh)], 'i','[0, nh - 1]'
            )
    prob.add_list_of_constraints(
        [(D*u)[1 + i * nu] >= -500 for i in range(0,nh)], 'i','[0, nh - 1]'
            )
    prob.add_list_of_constraints(
        [(D*u)[2 + i * nu] >= -500 for i in range(0,nh)], 'i','[0, nh - 1]'
            )
    prob.add_list_of_constraints(
        [(D*u)[0 + i * nu] <= 1000 for i in range(0,nh)], 'i','[0, nh - 1]'
            )
    prob.add_list_of_constraints(
        [(D*u)[1 + i * nu] <= 500 for i in range(0,nh)], 'i','[0, nh - 1]'
            )
    prob.add_list_of_constraints(
         [(D*u)[2 + i * nu] <= 500 for i in range(0,nh)], 'i','[0, nh - 1]'
            )


    ####y max/min constrains############
    prob.add_constraint( (Pu*u+x_part) >= 0 ) ###y(k+i) >= ymin
    prob.add_constraint( (Pu*u+x_part) <= 12000) ###y(k+i) >= ymin



    #####delta and z####################
    total_number = len(v1)
    total = nh*total_number
    prob.add_list_of_constraints([z1[i] == v1[i%total_number] * d1[i] for i in range(total)], 'i', '[total]')

    prob.add_list_of_constraints( 
         [pic.sum([d1[i * total_number + j] for j in range(nu8)], 'j', '[nu8]') == 1 for i in range(nh)],
         'i','[nh]')

    prob.add_list_of_constraints( 
          [pic.sum([d1[i * total_number + j] for j in range(nu8, total_number)], 'j', '[nu8, total_number]') == 1 
           for i in range(nh)],
          'i','[nh]')

    prob.add_list_of_constraints(
              [u[0 + i*nu] == pic.sum([z1[total_number * i + j] for j in range(nu8)], 'j', '[nu8]') 
                          for i in range(nh)], 'i','[nh]'
               )

    prob.add_list_of_constraints(
              [u[1 + i*nu] == pic.sum([z1[total_number * i + j] for j in range(nu8, total_number)], 'j', '[nu8, total_number]') 
                          for i in range(nh)], 'i','[nh]'
              )

    ############deta10 and z10###########


    y4max = 12000
    y4min = 0
    u8max = 10000
    u8min = 5000
    u9max = 500
    u9min = 0
    u10max = 500
    u10min = 0



    prob.add_list_of_constraints(
         [((Pu*u+x_part)[i - 1] - u[0 + i*nu] <= d10[i] * (y4max - u8min)) for i in range(1, nh)], 'i','[nh]'
             )
    prob.add_list_of_constraints(
         [((Pu*u+x_part)[i - 1] - u[0 + i*nu] >= (1 - d10[i]) * (y4min - u8max)) for i in range(1, nh)], 'i','[nh]'
             )
             

    prob.add_list_of_constraints(
          [(u[1 + (i - 1)*nu] - z10[i] <= (1 - d10[i]) * (u9max - u10min)) for i in range(1, nh)], 'i','[1, nh]'
              )

    prob.add_list_of_constraints(
          [(u[1 + (i - 1)*nu] - z10[i] >= (1 - d10[i]) * (u9min - u10max)) for i in range(1, nh)], 'i','[1, nh]'
              )

    prob.add_list_of_constraints(
         [z10[i] >= d10[i] * u10min for i in range(nh)], 'i','[1, nh]'
             )

    prob.add_list_of_constraints(
         [z10[i] <= d10[i] * u10max for i in range(nh)], 'i','[1, nh]'
             )

    prob.add_list_of_constraints(
         [u[2 + i * nu] == z10[i] for i in range(nh)], 'i','[1, nh]'
             )

    ur = ur
    
    J = cost_function(x_part, Pu, D, yr, ur, u9r, u, nu, ny, nh)
    prob.set_objective('min',J)
    prob.solve()
    return u

def modify_Csr(A, B, Csr, gamma64, tau6):
    A[4, 2] = (-gamma64 * Csr) * 1.0 / tau6
    B[4, 0] = gamma64*Csr * 1.0 / tau6
    
    


#######set parameter values##############
tau2 = 10
tau3 = 30
tau4 = 4
tau5 = 2
tau6 = 2

beta25 = 0.8
beta34 = 0.1
beta42 = 0.8
beta43 = 0.9
beta45 = 0.8
beta46 = 0.9
beta54 = 0.62

gamma29 = 2.5
gamma311 = 0.4
gamma510 = 0.6
gamma64 = 0.5




#################################
#create matrix A, B, C for
#  x(k+1) = A * x(k) + B * u(k)
#  y(k+1) = C * x(k)
#################################
A = np.zeros((5,5))
A[0, 0] = 1.0 - 1.0 / tau2
A[0, 3] = beta25 * 1.0 / tau2
A[1, 1] = 1.0 - 1.0 / tau3
A[1, 2] = (beta34 + gamma311) * 1.0 / tau3
A[2, 0] = beta42 * 1.0 / tau4
A[2, 1] = beta43 * 1.0 / tau4
A[2, 2] = 1.0 - 1.0 / tau4
A[2, 3] = beta45 * 1.0 / tau4
A[2, 4] = beta46 * 1.0 / tau4
A[3, 2] = beta54 * 1.0 / tau5
A[3, 3] = 1.0 - 1.0 / tau5
A[4, 2] = (-gamma64 * 0.96) * 1.0 / tau6
A[4, 4] = 1.0 - 1.0 / tau6


B = np.zeros((5,3))
B[0, 1] = gamma29 * 1.0 / tau2
B[1, 0] = -gamma311 * 1.0 / tau3
B[3, 2] = gamma510 * 1.0 / tau5
B[4, 0] = gamma64 * 0.96 * 1.0 / tau6


C = np.matrix('0, 0, 1, 0, 0')

nh = 7
Gx,Pu = predictive_model(A, B, C, nh)

nu8 = 6
nu9 = 6
###posible values of u8 and u9########
v1 = np.array([ 5000, 6000, 7000, 8000, 9000, 10000, 0, 100, 200, 300, 400, 500])

###inital state value########
xk = np.matrix('3000;0;5000;3000;0')

###desired behavior########
yr = np.matrix('10000')


###desired values of u8 and u9 in one predict round########
ur = [8000,8000,8000,8000,8000,8000,8000,8000]
u9r = [100,100,100,100,100,100,100,100]

nu = np.size(B[0, :])

penalty9 = 2.5

D_delta = difference_matrix_delta(nu, nh, penalty9)
D = difference_matrix(nu, nh)
ny = 1
u = HMPC(5000, 0, 0, Gx, xk, nu, ny, nh, nu8, nu9, D, Pu, ur, u9r, 0)



######loop to call the HMPC algorithm##########
y2 = np.zeros((1, 200))
y3 = np.zeros((1, 200))
y4 = np.zeros((1, 200))
y5 = np.zeros((1, 200))
y6 = np.zeros((1, 200))
Csrr = np.zeros((1,200))
u8 = np.zeros((1,200))
u9 = np.zeros((1,200))
u10 = np.zeros((1,200))
reached = 0


y2[0, 0] = xk[0]
y3[0, 0] = xk[1]
y4[0, 0] = xk[2]
y5[0, 0] = xk[3]
y6[0, 0] = xk[4]


u8[0, 0] = u[0].value[0, 0]
u9[0, 0] = u[1].value[0, 0]
u10[0, 0] = u[2].value[0, 0]


for i in range(199):
    print("##############", i)
    
    
    if i <= 10:
        ur = [5000,5000,5000,5000,6000,6000,6000,6000]
        u9r = [100,100,100,100,100,100,100,100]
    elif i <= 20 and i > 10:
        ur = [6000,6000,6000,7000,7000,7000,7000,7000]
        u9r = [200,200,200,200,200,200,200,200]
    elif i <= 40 and i > 20:
        ur = [7500,7500,7500,8000,8000,8000,8000,8000]
        u9r = [300,300,300,300,300,300,300,300]
    elif i <= 60 and i > 40:
        ur = [8500,8500,8500,9500,9500,9000,9000,9000]
        u9r = [500,500,500,500,500,500,500,500]
    elif i <= 100 and i > 60:
        ur = [9500,9500,9500,10000,10000,10000,10000,10000]
        u9r = [500,500,500,500,500,500,500,500]
    elif i >= 100:
        ur = [10000,10000,10000,10000,10000,10000,10000,10000]
        u9r = [300,300,300,300,300,300,300,300]

    Csr = 0.64 / 1.35 + 0.672 / 1.35 * np.exp(-0.2*i)
    Csrr[0, i] = Csr
    
    modify_Csr(A, B, Csr, gamma64, tau6)
    Gx,Pu = predictive_model(A, B, C, nh)
    
    u8_op = u[3].value[0,0]
    u9_op = u[4].value[0,0]
    u10_op = u[5].value[0,0]

    current_input = np.zeros((3,1))
    current_input[0,0] = u[0].value[0,0]
    current_input[1,0] = u[1].value[0,0]
    current_input[2,0] = u[2].value[0,0]
    xk = A.dot(xk) + B.dot(current_input)
    y2[0, i+1] = xk[0]
    y3[0, i+1] = xk[1]
    y4[0, i+1] = xk[2]
    y5[0, i+1] = xk[3]
    y6[0, i+1] = xk[4]
    
    add_constraint = 0
    
    
    
    if xk[2] >= 10000:
        reached += 1
        if reached >2:
            add_constraint = 1
    else:
        reached = 0
        add_constraint = 0
    
    u = HMPC(u8_op, u9_op, u10_op, Gx, xk, nu, ny, nh, nu8, nu9, D, Pu, ur, u9r, add_constraint)
    u8[0, i+1] = u[0].value[0, 0]
    u9[0, i+1] = u[1].value[0, 0]
    u10[0, i+1] = u[2].value[0, 0]


u8_step = np.zeros((1,201))
u9_step = np.zeros((1,201))
u10_step = np.zeros((1,201))

u8_step[0, 0] = u8[0, 0]
u8_step[0, 1:] = u8[0, :]

u9_step[0, 0] = u9[0, 0]
u9_step[0, 1:] = u9[0, :]

u10_step[0, 0] = u10[0, 0]
u10_step[0, 1:] = u10[0, :]



#############plot figures######################
t_u = range(201)
t = range(200)

xticks = range(0, 200, 20)

ax1 = plt.subplot2grid((5,1), (0,0))
ax1.step(t_u,u8_step[0, :],'b--')
plt.yticks([5000, 10000])
plt.xticks(xticks)
plt.title('Goals('+r'$\mu_8$' +')')
ax1.set_ylim((4000, 17000))

ax1 = plt.subplot2grid((5,1), (1,0))
ax1.step(t_u,u9_step[0, :],'b--')
plt.yticks([0, 200, 400])
plt.xticks(xticks)
plt.title('Outcome Expectancy for Reinforcement (Available points ' + '' r'$\xi_9$' + '=' + r'$\mu_9$'+')')
ax1.set_ylim((-100, 600))


ax1 = plt.subplot2grid((5,1), (2,0))
ax1.step(t_u,u10_step[0, :],'b--')
plt.title('Reinforcement Granted Points (' + r'$\xi_{10}$' + '='+r'$\mu_{10}$' + ')')
plt.yticks([0, 200,400])
plt.xticks(xticks)
ax1.set_ylim((-100, 600))

ax1 = plt.subplot2grid((5,1), (3,0), rowspan=2)
ax1.plot(t,y4[0, :],'k-', label = 'daily steps ('+r'$y_4$' +'=' + r'$\eta_4$'+')')
ax1.step(t,u8[0, :],'b--', label = 'Step goals('+r'$\mu_8$'+')')
plt.xlabel('time (day)')
plt.title('Behavior' + '(' r'$\eta_4$' + ')')
ax1.set_ylim((0, 12000))
plt.xticks(range(0, 200, 20))
plt.axhline(y=10000, linestyle = '--', dashes=(2,2), color = 'r', label = 'Desired behavior(' + r'$y_{des}$'+')')

plt.tight_layout(pad=1, w_pad=0.5, h_pad=1)
plt.legend(loc='lower right', ncol=1, prop={'size':10})

plt.show()






