##########################################################################
# Implementation our Migration Model
# The scenario is  non-government and no natural disaster
##########################################################################

import picos as pic
import numpy as np
import matplotlib.pyplot as plt
import math


def predictive_model(A, B, C, p):
        
        ###########################################
        #Y(k+1...k+p) = G * x(k) + H * U(k...K+p-1)
        ###########################################
        nC = np.size(C[:, 0])
        nB = np.size(B[0, :])
        nA = np.size(A[0, :])
        H = np.zeros(( nC * p, nB * p))
        P = C
        G = np.zeros(( nC * p, nA))

        for i in range(p):  
            H_temp = P.dot(B)
            for j in range(i, p):
                H[j * nC : (j + 1) * nC, (j - i) * nB : (j - i + 1) * nB] = H_temp
            P = P.dot(A)
            G[i*nC:(i+1)*nC] = P

        return G, H 

    
def cost_function( x_part, H, D, yr, ur, u5r, u6r,  u, nu, ny, p):

        ######delta_Y_Yr##############
        Qy = np.zeros((p * ny, p * ny))
        
        for i in range(p):
            Qy[3 + (i*ny), 3 + (i*ny)] = 1
        
        delta_Yr = ((H*u+x_part - yr).T) * Qy * (H*u + x_part - yr)

        #####delta_Uk_Uk+1############
        delta_u = u.T * D_delta.T * D_delta * u

        #####delta_U_Ur###############
        delta_Ur = (u[7] - ur[0]) * (u[7] - ur[0]) * 2
        delta_Ur += (u[5] - u6r[0]) * (u[5] - u6r[0])
        delta_Ur += (u[4] - u5r[0]) * (u[4] - u5r[0])
        for i in range(1, p): 
            delta_Ur += (u[7 + i * nu] - ur[i]) * (u[7 + i * nu] - ur[i]) * 2
            delta_Ur += (u[5 + i * nu] - u6r[i]) * (u[5 + i * nu] - u6r[i])
            delta_Ur += (u[4 + i * nu] - u5r[i]) * (u[4 + i * nu] - u5r[i])
        
        return (delta_Yr )+ delta_Ur + delta_u
    
    
def difference_matrix_delta( nu, nh, penalty9):
        D = np.zeros((nu * nh, nu * nh))
        for i in range(nh - 1):
            temp = np.eye(nu)
            temp[1,1] = penalty9
            D[i * nu : (i+1) * nu, i * nu : (i+1) * nu] = temp
            D[i * nu : (i+1) * nu, (i+1) * nu : (i+2) * nu] = -temp
        return D

def difference_matrix( nu, nh):
    D = np.zeros((nu * nh, nu * nh))
    for i in range(nh - 1):
        temp = np.eye(nu)
        D[i * nu : (i+1) * nu, i * nu : (i+1) * nu] = temp
        D[i * nu : (i+1) * nu, (i+1) * nu : (i+2) * nu] = -temp
    return D



#####implementation of HMPC algorithm#############
def HMPC(u_0, Gx, xk, nu, ny, nh, nu1, nu2, D, Pu, ur, u5r, u6r, add_constraint):

    prob = pic.Problem()
    
    u = prob.add_variable('u',(nh * nu, 1))
    d1 = prob.add_variable('d1',nh * len(v1), vtype='binary')
    z1 = prob.add_variable('z1',nh * len(v1))
    d4 = prob.add_variable('d4',nh, vtype='binary')
    z4 = prob.add_variable('z4',nh)
    
    D = pic.new_param('D',D)
    Pu = pic.new_param('Pu',Pu)
    
    x_part = Gx.dot(xk)
    x_part = pic.new_param('x_part',x_part)

#     ###initial value constrains
    prob.add_constraint(u[0]==u_0[0])
    prob.add_constraint(u[1]==u_0[1])
    prob.add_constraint(u[2]==u_0[2])
    prob.add_constraint(u[3]==u_0[3])
    prob.add_constraint(u[4]==u_0[4])
    prob.add_constraint(u[5]==u_0[5])
    prob.add_constraint(u[6]==u_0[6])
    prob.add_constraint(u[7]==u_0[7])
    

    ###### U(k) <= |100| and U(k) >= |0|

    prob.add_list_of_constraints(
        [u >= 0 for i in range(nh)], 'i','[0, nh]'
            )
    prob.add_list_of_constraints(
        [u <= 100 for i in range(nh)], 'i','[0, nh]'
            )


    ####y max/min constrains###############
    prob.add_constraint( (Pu*u+x_part) >= 0 ) ###y(k+i) >= ymin
    prob.add_constraint( (Pu*u+x_part) <= 120) ###y(k+i) >= ymin
    

    ####delta and z############
    total_number = len(v1)
    total = nh*total_number
    prob.add_list_of_constraints([z1[i] == v1[i%total_number] * d1[i] for i in range(total)], 'i', '[total]')

    prob.add_list_of_constraints( 
         [pic.sum([d1[i * total_number + j] for j in range(nu1)], 'j', '[nu1]') == 1 for i in range(nh)],
         'i','[nh]')

    prob.add_list_of_constraints( 
          [pic.sum([d1[i * total_number + j] for j in range(nu1, nu1+nu2)], 'j', '[nu1, nu1+nu2]') == 1 
           for i in range(nh)],
          'i','[nh]')
    
    prob.add_list_of_constraints( 
          [pic.sum([d1[i* total_number + j] for j in range(nu1+nu2, total_number)], 'j', '[index, total_number]') == 1 
           for i in range(1)],
          'i','[nh]')

    prob.add_list_of_constraints(
              [u[0 + i*nu] == pic.sum([z1[total_number * i + j] for j in range(nu1)], 'j', '[nu1]') 
                          for i in range(nh)], 'i','[nh]'
               )

    prob.add_list_of_constraints(
              [u[1 + i*nu] == pic.sum([z1[total_number * i + j] for j in range(nu1, nu1 + nu2)], 'j', '[nu1, nu1+nu2]') 
                          for i in range(nh)], 'i','[nh]'
              )

    prob.add_list_of_constraints(
              [u[6 + i*nu] == pic.sum([z1[total_number * i + j] for j in range(nu1 + nu2, total_number)], 'j', '[nu1+nu2, total_number]') 
                          for i in range(nh)], 'i','[nh]'
              )
              
              
    ############deta4 and z4################
    y4max = 100
    y4min = 0
    u8max = 100
    u8min = 0
    u9max = 100
    u9min = 0
    u4max = 100
    u4min = 0
    u9 = 50
         

    prob.add_list_of_constraints(
         [(u[7 + i*nu] - (Pu*u+x_part)[3 + (i - 1) * ny]  <= d4[i] * (u8max - y4min)) for i in range(1, nh)], 'i','[nh]'
             )
    prob.add_list_of_constraints(
         [(u[7 + i*nu] - (Pu*u+x_part)[3 + (i - 1) * ny]  >= (1 - d4[i]) * (u8min - y4max)) for i in range(1, nh)], 'i','[nh]'
             )
    prob.add_list_of_constraints(
          [( 0 - z4[i] <= (1 - d4[i]) * (u9max - u4min)) for i in range(1, nh)], 'i','[1, nh]'
              )
    prob.add_list_of_constraints(
          [(0 - z4[i] >= (1 - d4[i]) * (u9min - u4max)) for i in range(1, nh)], 'i','[1, nh]'
              )

    prob.add_list_of_constraints(
         [z4[i] >= d4[i] * u4min for i in range(nh)], 'i','[1, nh]'
             )

    prob.add_list_of_constraints(
         [z4[i] <= d4[i] * u4max for i in range(nh)], 'i','[1, nh]'
             )

    prob.add_list_of_constraints(
         [u[3 + i * nu] == z4[i] for i in range(nh)], 'i','[1, nh]'
             )

    ur = ur
    
    J = cost_function(x_part, Pu, D, yr, ur, u5r, u6r, u, nu, ny, nh)
    prob.set_objective('min',J)
    prob.solve()
    return u, d1, z1


def modify_Csr(A, B):
    A[2, 3] = (beta34 - 0) * 1.0 / tau3
    B[2, 7] = 0 * 1.0 / tau3
    
    


#######set parameter values##############
tau1 = 0.8
tau2 = 0.9
tau3 = 5
tau4 = 0.5
tau5 = 5
tau6 = 8

beta14 = 0.3
beta21 = 0.5
beta25 = 0.5
beta31 = 0.3
beta34 = 0.4
beta42 = 0.5
beta43 = 0.8
beta46 = 0.3
beta42 = 0.2
beta45 = 0.3
beta54 = 0.23

gamma11 = 0.1
gamma12 = 0.12
gamma17 = 0.13
gamma26 = 0.1
gamma36 = 0.1
gamma38 = 0.9
gamma55 = 0.5
gamma64 = 0.3
gamma63 = 0.3

#################################
#create matrix A, B, C for
#  x(k+1) = A * x(k) + B * u(k)
#  y(k+1) = C * x(k)
#################################
A = np.zeros((6,6))
A[0, 0] = 1.0 - 1.0 / tau1
A[0, 3] = beta14 * 1.0 / tau1
A[1, 0] = beta21 * 1.0 / tau2
A[1, 1] = 1.0 - 1.0 / tau2
A[1, 2] = beta25* 1.0 / tau2
A[2, 0] = beta31 * 1.0 / tau3
A[2, 2] = 1.0 - 1.0 / tau3
A[2, 3] = (beta34 - gamma38) * 1.0 / tau3
A[3, 1] = beta42 * 1.0 / tau4
A[3, 2] = beta43 * 1.0 / tau4
A[3, 3] = 1.0 - 1.0 / tau4
A[3, 4] = beta45 * 1.0 / tau4
A[3, 5] = beta46 * 1.0 / tau4
A[4, 3] = beta54 * 1.0 / tau5
A[4, 4] = 1.0 - 1.0 / tau5
A[5, 5] = 1.0 - 1.0 / tau6


B = np.zeros((6,8))
B[0, 0] = gamma11 * 1.0 / tau1
B[0, 1] = gamma12 * 1.0 / tau1
B[0, 6] = gamma17 * 1.0 / tau1
B[1, 5] = gamma26 * 1.0 / tau2
B[2, 5] = gamma36 * 1.0 / tau3
B[2, 7] = gamma38 * 1.0 / tau3
B[4, 4] = gamma55 * 1.0 / tau5
B[5, 2] = gamma63 * 1.0 / tau6
B[5, 3] = gamma64 * 1.0 / tau6

ny = 6
nh = 7
C = np.eye(ny)
Gx,Pu = predictive_model(A, B, C, nh)

nu1 = 4
nu2 = 5
nu7 = 6
###posible values of u1 and u2########
v1 = np.array([ 44.5, 96, 86.7, 72.8, 96.5, 59, 76.3, 83.2, 85, 89.6, 77.5, 72.3, 79.8, 83.8, 97.1])

###inital state value########
xk = np.matrix('10;20;20;50;50;0')

###desired behavior########
yr = np.matrix('100')

###desired values of u8 and u9 in one predict round########
#ur  = [80,80,80,80,80,80,80]
ur  = [100,100,100,100,100,100,100]
u5r = [90,90,90,90,90,90,90]
u6r = [90,90,90,90,90,90,90]

nu = np.size(B[0, :])

penalty9 = 1

D_delta = difference_matrix_delta(nu, nh, penalty9)
D = difference_matrix(nu, nh)

u_0 = [96, 85, 0, 30, 100, 90, 72.3, 80]
u, d1, z1 = HMPC(u_0, Gx, xk, nu, ny, nh, nu1, nu2, D, Pu, ur, u5r, u6r, 0)


print u



######loop to call the HMPC algorithm##########
y1 = np.zeros((1, 120))
y2 = np.zeros((1, 120))
y3 = np.zeros((1, 120))
y4 = np.zeros((1, 120))
y5 = np.zeros((1, 120))
y6 = np.zeros((1, 120))
Csrr = np.zeros((1,120))
u1 = np.zeros((1,120))
u2 = np.zeros((1,120))
u3 = np.zeros((1,120))
u4 = np.zeros((1,120))
u5 = np.zeros((1,120))
u6 = np.zeros((1,120))
u7 = np.zeros((1,120))
u8 = np.zeros((1,120))
reached = 0

y1[0, 0] = xk[0]
y2[0, 0] = xk[1]
y3[0, 0] = xk[2]
y4[0, 0] = xk[3]
y5[0, 0] = xk[4]
y6[0, 0] = xk[5]


u1[0, 0] = u[0].value[0,0]
u2[0, 0] = u[1].value[0,0]
u3[0, 0] = u[2].value[0,0]
u4[0, 0] = u[3].value[0,0]
u5[0, 0] = u[4].value[0,0]
u6[0, 0] = u[5].value[0,0]
u7[0, 0] = u[6].value[0,0]
u8[0, 0] = u[7].value[0,0]


for i in range(119):
    print("##############", i)
    
    if i <= 10:
        # ur  = [80,80,80,80,80,80,80]
         u6r = [90,90,90,90,90,90,90]
         u5r = [90,90,90,90,90,90,90]
    
    
    elif i <= 20 and i > 10:
        #  ur  = [80,80,80,80,80,80,80]
         u6r = [80,80,80,80,80,80,80]
         u5r = [80,80,80,80,80,80,80]
    
    elif i <= 40 and i > 20:
        #  ur  = [80,80,80,80,80,80,80]
         u6r = [70,70,70,70,70,70,70]
         u5r = [80,80,80,80,80,80,80]

    elif i <= 60 and i > 40:
        #  ur  = [80,80,80,80,80,80,80]
         u6r = [60,60,60,60,60,60,60]
         u5r = [80,80,80,80,80,80,80]

    elif i > 60:
        # ur  = [80,80,80,80,80,80,80]
         u6r = [50,50,50,50,50,50,50]
         u5r = [80,80,80,80,80,80,80]
    
    Csr = 0.64 / 1.35 + 0.672 / 1.35 * np.exp(-0.2*i)
    Csrr[0, i] = Csr
    
#    if (i > 7 and i < 10) or (i > 19 and i < 22) or (i > 31 and i < 34) or (i > 43 and i < 46) or (i > 55 and i <  58):
    modify_Csr(A, B)        #no government support to the self-efficacy

    Gx,Pu = predictive_model(A, B, C, nh)
    
    u_op1 = 96
    u_op2 = 85
    u_op3 = u[10].value[0,0]
    u_op4 = u[11].value[0,0]
    u_op5 = u[12].value[0,0]
    u_op6 = u[13].value[0,0]
    u_op7 = 72.3
    u_op8 = u[15].value[0,0]
    
#####nature disaster ##########
#    if (i > 7 and i < 58):
#        u_op5 = 0
#        u_op3 = 10
#        u_op6 = 10

    u_op = [u_op1, u_op2, u_op3, u_op4, u_op5, u_op6, u_op7, u_op8]

    current_input = np.zeros((8,1))
    current_input[0,0] = u[0].value[0,0]
    current_input[1,0] = u[1].value[0,0]
    current_input[2,0] = u[2].value[0,0]
    current_input[3,0] = u[3].value[0,0]
    current_input[4,0] = u[4].value[0,0]
    current_input[5,0] = u[5].value[0,0]
    current_input[6,0] = u[6].value[0,0]
    current_input[7,0] = u[7].value[0,0]
    
    xk = A.dot(xk) + B.dot(current_input)
    y1[0, i+1] = xk[0]
    y2[0, i+1] = xk[1]
    y3[0, i+1] = xk[2]
    y4[0, i+1] = xk[3]
    y5[0, i+1] = xk[4]
    y6[0, i+1] = xk[5]
    
    add_constraint = 0
    
    u, d1, z1 = HMPC(u_op, Gx, xk, nu, ny, nh, nu1, nu2, D, Pu, ur, u5r, u6r, add_constraint)
    u1[0, i+1] = u[0].value[0,0]
    u2[0, i+1] = u[1].value[0,0]
    u3[0, i+1] = u[2].value[0,0]
    u4[0, i+1] = u[3].value[0,0]
    u5[0, i+1] = u[4].value[0,0]
    u6[0, i+1] = u[5].value[0,0]
    u7[0, i+1] = u[6].value[0,0]
    u8[0, i+1] = u[7].value[0,0]


#############plot figures######################
t = range(120)

yticks = range(0, 120, 20)
xticks = range(0, 120, 20)
yticks2 = range(0, 120, 40)

ax1 = plt.subplot2grid((5,2), (0,0))
ax1.plot(t,u4[0, :],'b-')
plt.title('External Cue' + '(' r'$\mu_4$' + ')')
plt.yticks(yticks2)
plt.xticks(xticks)
ax1.set_ylim((0, 120))

ax1 = plt.subplot2grid((5,2), (0,1))
ax1.plot(t,u5[0, :],'b-')
plt.title('Environmental Context' + '(' r'$\mu_5$' + ')')
plt.yticks(yticks2)
plt.xticks(xticks)
ax1.set_ylim((0, 120))

ax1 = plt.subplot2grid((5,2), (1,0))
ax1.plot(t,u6[0, :],'b-')
plt.title('Observed Migration' + '(' r'$\mu_6$' + ')')
plt.yticks(yticks2)
plt.xticks(xticks)
ax1.set_ylim((0, 120))

ax1 = plt.subplot2grid((5,2), (1,1))
ax1.plot(t,u8[0, :],'b-')
plt.title('Goal' + '(' r'$\mu_8$' + ')')
plt.yticks(yticks2)
plt.xticks(xticks)
ax1.set_ylim((0, 120))


ax1 = plt.subplot2grid((5,2), (2,0))
ax1.plot(t,y2[0, :],'b-')
plt.title('Satisfactory Expectancy' + '(' r'$\eta_2$' + ')')
plt.yticks(yticks2)
plt.xticks(xticks)
ax1.set_ylim((0, 120))


ax1 = plt.subplot2grid((5,2), (2,1))
ax1.plot(t,y3[0, :],'b-')
plt.title('Confidence' + '(' r'$\eta_3$' + ')')
plt.yticks(yticks2)
plt.xticks(xticks)
ax1.set_ylim((0, 120))


ax1 = plt.subplot2grid((5,2), (3,0), rowspan=2)
ax1.plot(t,y5[0, :],'b-')
plt.title('Satisfactory of Staying' + '(' r'$\eta_5$' + ')')
plt.yticks(yticks)
plt.xticks(xticks)
plt.xlabel('time (month)')
ax1.set_ylim((0, 120))

ax1 = plt.subplot2grid((5,2), (3,1), rowspan=2)
ax1.plot(t,y4[0, :],'k-', label = 'daily steps ('+r'$y_4$' +'=' + r'$\eta_4$'+')')
ax1.plot(t,u8[0, :],'b--', dashes = (2,2))
plt.xlabel('time (month)')
plt.title('Behavior' + '(' r'$\eta_4$' + ')')
ax1.set_ylim((0, 120))
plt.yticks(yticks)
plt.xticks(xticks)
plt.axhline(y=100, linestyle = '--', dashes=(2,2), color = 'r', label = 'Desired behavior(' + r'$y_{des}$'+')')

plt.tight_layout(pad=1, w_pad=0.5, h_pad=1)
plt.show()
