import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from pydelay import dde23


######delay model using dde23################
params = {
    
    'tau0' : 1,
    'tau1' : 1,
    'tau2' : 1,
    'tau3' : 2,
    'tau4' : 1,
    'tau5' : 3,
    'gamma00' : 3,
    'gamma11' : 1,
    'gamma21' : 2,
    'gamma22' : 1,
    'gamma24' : 1,
    'gamma25' : 1,
    'gamma46' : 2,
    'gamma53' : 15,
    'gamma57' : 15,
    'beta10' : 0.3,
    'beta20' : 0.5,
    'beta31' : 0.3,
    'beta32' : 0.8,
    'beta43' : 0.3,
    'beta23' : 0.2,
    'beta14' : 0.3,
    'beta03' : 0.23,
    'beta35' : 0.44,
    'beta34' : 0.1,
    'tt1' : 0.0,
    'tt2' : 0.0,
    'tt3' : 0.0,
    'tt4' : 0.0,
    'tt5' : 0.0,
    'tt6' : 0.0,
    'tt7' : 0.0,
    'tt8' : 0.0,
    'tt9' : 0.0,
    'tt_10': 0.0,
    'tt_11': 0.0,
    'tt_12': 0.0,
    'tt_13': 0.0,
    'tt_14': 0.0,
    'tt_15': 0.0,
    'tt_16': 0.0,
    'tt_17': 1,
    'tt_18': 1,
    'tt_21': 0.0
}

mycode = """


double xi1(double t){return 0.0;}    
double xi2(double t){return 10.0;}
double xi3(double t){return 10.0;}
double xi4(double t){return 0.0;}

double xi5(double t){return 2.0;}
double xi6(double t){return 0.0;}
double xi7(double t){return 0.0;}
double xi8(double t){
    if (t <= 14  && t >= 1){return 7;} 
    else{return 0;}       
}


double zeta1(double t){return 0.0;}
double zeta2(double t){return 0.0;}
double zeta3(double t){return 0.0;}
double zeta4(double t){return 0.0;}
double zeta5(double t){return 0.0;}
double zeta6(double t){return 0.0;}


double fun_deta35(double h, int habitation){

double beta_35 = 0.44;

if (habitation == 1){

if (h > 95){beta_35 = 0;}
else if (h > 90){beta_35 = 0.4;}

}

return beta_35;
}
"""
histfunc = {
     'h0': lambda t: 20,
     'h1': lambda t: 20,
     'h2': lambda t: 50,
     'h3': lambda t: 50,
     'h4': lambda t: 50,
     'h5': lambda t: 0.0,
     }




eqns_without_habitation = {
    
     'h0': '(gamma00 * xi1(t - tt1) + beta03*h3(t - tt_16) - h0 + zeta1(t)) / tau0' ,
     'h1': '(gamma11 * xi2(t - tt4) + beta10*h0(t - tt2) + beta14 * h4(t - tt_14) - h1 + zeta2(t)) / tau1',
     'h2': '(gamma21 * xi2(t - tt5) + gamma22 * xi3(t - tt7) - gamma24 * xi5(t - tt9) + gamma25*xi6(t - tt_10) + beta20 * h0(t - tt3) + beta23*h3(t - tt_13) - h2 + zeta3(t))/tau2',
     'h3': '(beta31*h1(t - tt6) + beta32*h2(t - tt8) + fun_deta35(h5, 0) * h5(t - tt_17) + beta34 * h4(t - tt_21) - h3 + zeta4(t))/tau3',
     'h4': '(gamma46 * xi7(t - tt_15) + beta43*h3(t - tt_12) - h4 + zeta5(t)) / tau4',
     'h5': '(gamma53 * xi4(t - tt_11) + gamma57*xi8(t - tt_18) - h5 + zeta6(t)) / tau5'
}
eqns_without_habitation = dde23(eqns=eqns_without_habitation, params = params, supportcode=mycode)
eqns_without_habitation.set_sim_params(tfinal=20, dtmax=0.2)
eqns_without_habitation.hist_from_funcs(histfunc)
eqns_without_habitation.run()


eqns_with_habitation = {
    
     'h0': '(gamma00 * xi1(t - tt1) + beta03*h3(t - tt_16) - h0 + zeta1(t)) / tau0' ,
     'h1': '(gamma11 * xi2(t - tt4) + beta10*h0(t - tt2) + beta14 * h4(t - tt_14) - h1 + zeta2(t)) / tau1',
     'h2': '(gamma21 * xi2(t - tt5) + gamma22 * xi3(t - tt7) - gamma24 * xi5(t - tt9) + gamma25*xi6(t - tt_10) + beta20 * h0(t - tt3) + beta23*h3(t - tt_13) - h2 + zeta3(t))/tau2',
     'h3': '(beta31*h1(t - tt6) + beta32*h2(t - tt8) + fun_deta35(h5, 1) * h5(t - tt_17) + beta34 * h4(t - tt_21) - h3 + zeta4(t))/tau3',
     'h4': '(gamma46 * xi7(t - tt_15) + beta43*h3(t - tt_12) - h4 + zeta5(t)) / tau4',
     'h5': '(gamma53 * xi4(t - tt_11) + gamma57*xi8(t - tt_18) - h5 + zeta6(t)) / tau5'
}
eqns_with_habitation = dde23(eqns=eqns_with_habitation, params = params, supportcode=mycode)
eqns_with_habitation.set_sim_params(tfinal=20, dtmax=0.2)
eqns_with_habitation.hist_from_funcs(histfunc)
eqns_with_habitation.run()

sol_with = eqns_with_habitation.sample(0, 20, 0.1)
y6_with = sol_with['h5']
y4_with = sol_with['h3']

sol_without = eqns_without_habitation.sample(0, 20, 0.1)
y6_without = sol_without['h5']
y4_without = sol_without['h3']
t = sol_without['t']




##############plot figure##########
def xi8(t):
    if t >= 1 and t <= 14:
        return 7
    else:
        return 0

u8_s3 = np.vectorize(xi8)
vu8_s3 = u8_s3(t)


ax1 = plt.subplot(3, 1, 1)
ax1.plot(t,vu8_s3,'b-')
ax1.plot(t,vu8_s3,'r--')
plt.xlabel('time (day)')
plt.title('External Cue' + '(' r'$\xi_8$' + ")")
ax1.set_ylim((0, 10))

ax1 = plt.subplot(3, 1, 2)
ax1.plot(t,y6_with,'r--')
ax1.plot(t,y6_without,'b-')
plt.xlabel('time (day)')
plt.title('Cue to Action' + '(' r'$\eta_6$' + ")")
ax1.axvspan(1, 2, alpha=0.3, color='green')
ax1.set_ylim((0, 100))

ax1 = plt.subplot(3, 1, 3)
ax1.plot(t,y4_with,'r--', label = 'with habituation')
ax1.plot(t,y4_without,'b-', label = 'without habituation')
plt.axhline(y=50, linestyle = '--', dashes=(2,2), color = 'g')
plt.xlabel('time (day)')
plt.title('Behavior' + '(' r'$\eta_4$' + ")")
ax1.axvspan(1, 3, alpha=0.3, color='green')
ax1.axvline(2, color='green', lw=2, alpha=0.3)
ax1.set_ylim((0, 100))

plt.tight_layout(pad=1, w_pad=0.5, h_pad=0.8)
plt.legend(loc=9, bbox_to_anchor=(0.79, -0.2), ncol=2, prop={'size':10}, columnspacing = 0.6)

plt.show()