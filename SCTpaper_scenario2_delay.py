import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from pydelay import dde23


######delay##########
eqns = {
    
     'h0': '(gamma00 * xi1(t - tt1) + beta03*h3(t - tt_16) - h0 + zeta1(t)) / tau0' ,
     'h1': '(gamma11 * xi2(t - tt4) + beta10*h0(t - tt2) + beta14 * h4(t - tt_14) - h1 + zeta2(t)) / tau1',
     'h2': '(gamma21 * xi2(t - tt5) + gamma22 * xi3(t - tt7) - gamma24 * xi5(t - tt9) + gamma25*xi6(t - tt_10) + beta20 * h0(t - tt3) + beta23*h3(t - tt_13) - h2 + zeta3(t))/tau2',
     'h3': '(beta31*h1(t - tt6) + beta32*h2(t - tt8) + beta35 * h5(t - tt_17) + beta34 * h4(t - tt_21) - h3 + zeta4(t))/tau3',
     'h4': '(gamma46 * xi7(t - tt_15) + beta43*h3(t - tt_12) - h4 + zeta5(t)) / tau4',
     'h5': '(gamma53 * xi4(t - tt_11) + gamma57*xi8(t - tt_18) - h5 + zeta6(t)) / tau5'
}

params = {
    
    'tau0' : 1,
    'tau1' : 1,
    'tau2' : 1,
    'tau3' : 2,
    'tau4' : 1,
    'tau5' : 3,
    'gamma00' : 3,
    'gamma11' : 1,
    'gamma21' : 2,
    'gamma22' : 1,
    'gamma24' : 1,
    'gamma25' : 1,
    'gamma46' : 2,
    'gamma53' : 15,
    'gamma57' : 15,
    'beta10' : 0.3,
    'beta20' : 0.5,
    'beta31' : 0.3,
    'beta32' : 0.8,
    'beta43' : 0.3,
    'beta23' : 0.2,
    'beta14' : 0.3,
    'beta03' : 0.23,
    'beta35' : 0.44,
    'beta34' : 0.1,
    'tt1' : 0.0,
    'tt2' : 0.0,
    'tt3' : 0.0,
    'tt4' : 0.0,
    'tt5' : 0.0,
    'tt6' : 0.0,
    'tt7' : 0.0,
    'tt8' : 0.0,
    'tt9' : 0.0,
    'tt_10': 0.0,
    'tt_11': 0.0,
    'tt_12': 0.0,
    'tt_13': 0.0,
    'tt_14': 0.0,
    'tt_15': 0.0,
    'tt_16': 0.0,
    'tt_17': 1,
    'tt_18': 1,
    'tt_21': 0.0
}

mycode = """


double xi1(double t){return 0.0;}    
double xi2(double t){return 10.0;}
double xi3(double t){return 10.0;}
double xi4(double t){

if ( (t >= 14 && t <=15) || (t>=16 && t<=17) || (t >=18 && t <=19)){return 5;} 
    else{return 0;}
}

double xi5(double t){return 2.0;}
double xi6(double t){return 0.0;}
double xi7(double t){return 0.0;}
double xi8(double t){
    if (t < 2 || t > 12 || (t >6 && t <8)){return 0;} 
    else{return 5;}       
}


double zeta1(double t){return 0.0;}
double zeta2(double t){return 0.0;}
double zeta3(double t){return 0.0;}
double zeta4(double t){return 0.0;}
double zeta5(double t){return 0.0;}
double zeta6(double t){return 0.0;}
"""


dde = dde23(eqns=eqns, params = params, supportcode=mycode)
dde.set_sim_params(tfinal=20, dtmax=0.2)
histfunc = {
     'h0': lambda t: 10,
     'h1': lambda t: 20,
     'h2': lambda t: 50,
     'h3': lambda t: 50,
     'h4': lambda t: 25,
     'h5': lambda t: 0.0,
     }

dde.hist_from_funcs(histfunc)
dde.run()
sol1 = dde.sample(0, 20, 0.1)

y2 = sol1['h1']
y3 = sol1['h2']
y4 = sol1['h3']
y6 = sol1['h5']
t = sol1['t']






##############plot figure##########
def xi8(t):
    
    if t < 2 or t > 12 or (t >6 and t <8):
        return 0
    else:
        return 5
    
    
def xi4(t):
    if  (t >= 14 and t <=15) or (t>=16 and t<=17) or (t >=18 and t <=19):
        return 5
    else:
        return 0


u8_s2 = np.vectorize(xi8)
vu8_s2 = u8_s2(t)

u4_s2 = np.vectorize(xi4)
vu4_s2 = u4_s2(t)




ax1 = plt.subplot(3, 2, 1)
ax1.plot(t,vu4_s2,'-')
plt.xlabel('time (day)')
plt.title('Internal Cue' + '(' r'$\xi_4$' + ")")
ax1.set_ylim((0, 10))

ax1 = plt.subplot(3, 2, 2)
ax1.plot(t,vu8_s2,'-')
plt.xlabel('time (day)')
plt.title('External Cue' + '(' r'$\xi_8$' + ")")
ax1.set_ylim((0, 10))

ax1 = plt.subplot(3, 2, 3)
ax1.plot(t,y3,'-')
plt.xlabel('time (day)')
plt.title('self-efficacy' + '(' r'$\eta_3$' + ")")
ax1.set_ylim((0, 100))


ax1 = plt.subplot(3, 2, 4)
ax1.plot(t,y6,'-')
plt.xlabel('time (day)')
plt.title('Cue to Action' + '(' r'$\eta_6$' + ")")
ax1.axvspan(2, 3, alpha=0.3, color='green')
ax1.set_ylim((0, 100))

ax1 = plt.subplot(3, 2, 5)
ax1.plot(t,y2,'-')
plt.xlabel('time (day)')
plt.title('Outcome Expectancy' + '(' r'$\eta_2$' + ")")
ax1.set_ylim((0, 100))


ax1 = plt.subplot(3, 2, 6)
ax1.plot(t,y4,'-', label = 'scenario2')
plt.axhline(y=50, linestyle = '--', dashes=(2,2), color = 'r')
plt.xlabel('time (day)')
plt.title('Behavior' + '(' r'$\eta_4$' + ")")
ax1.axvspan(2, 4, alpha=0.3, color='green')
ax1.axvline(3, color='green', lw=2, alpha=0.3)
ax1.set_ylim((0, 100))

plt.tight_layout(pad=1, w_pad=0.5, h_pad=0.8)
plt.legend(loc=9, bbox_to_anchor=(-0.08, -0.2), ncol=2, prop={'size':11})
plt.show()