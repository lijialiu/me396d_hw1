
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from scipy.integrate import odeint

####no delay#######
def tank(h, t, tau, gamma, beta):
    dh1dt = (gamma[0][0] * xi1(t) + beta[0][3]*h[3]-h[0] + zeta1(t)) / tau[0]
    dh2dt = (gamma[1][1] * xi2(t) + beta[1][0]*h[0] + beta[1][4] * h[4] - h[1] + zeta2(t)) / tau[1]
    dh3dt = (gamma[2][1] * xi2(t) + gamma[2][2] * xi3(t) - gamma[2][4] * xi5(t) +
            gamma[2][5]*xi6(t) + beta[2][0] * h[0]+
            beta[2][3]*h[3] - h[2] + zeta3(t))/tau[2]
    
    dh4dt = (beta[3][1]*h[1] + beta[3][2]*h[2] + beta[3][5] * h[5] +
            beta[3][4] * h[4] - h[3] + zeta4(t))/tau[3]
    
    dh5dt = (gamma[4][6] * xi7(t) + beta[4][3]*h[3] - h[4] + zeta5(t)) / tau[4]
    dh6dt = (gamma[5][3] * xi4(t) + gamma[5][7]*xi8(t) - h[5] + zeta6(t)) / tau[5]
    
    dhdt = [dh1dt, dh2dt, dh3dt, dh4dt, dh5dt, dh6dt]
    
    return dhdt

#####parameters######

####time constants#######
def define_tau(num):
    tau = np.zeros(num)
    return tau

#####input/output resistance######
def define_matrix(num):
    gamma = np.zeros((num,num))
    return gamma

####input########
def xi1(t):
    return 0
def xi2(t):
    return 3
def xi3(t):
    return 3
def xi4(t):
    return 0
def xi5(t):
    return 10
def xi6(t):
    return 0
def xi7(t):
    return 0
def xi8(t):
    
    if t < 2 or t > 12 or (t >6 and t <8):
        return 0
    else:
        return 5

####noise######
def zeta1(t):
    return 0.0
def zeta2(t):
    return 0.0
def zeta3(t):
    return 0.0
def zeta4(t):
    return 0.0
def zeta5(t):
    return 0.0
def zeta6(t):
    return 0.0


tau = define_tau(6)
tau[0:3] = 1
tau[3] = 2
tau[4] = 1
tau[5] = 3

gamma = define_matrix(8)
gamma[0][0] = 3
gamma[1][1] = 1
gamma[2][1] = 2
gamma[2][2] = 1
gamma[2][4] = 1
gamma[2][5] = 1
gamma[4][6] = 2
gamma[5][3] = 15
gamma[5][7] = 15

beta = define_matrix(6)
beta[1][0] = 0.3
beta[2][0] = 0.5
beta[3][1] = 0.3
beta[3][2] = 0.8
beta[4][3] = 0.3
beta[2][3] = 0.2
beta[1][4] = 0.3
beta[0][3] = 0.23
beta[3][5] = 0.44
beta[3][4] = 0.1

t = np.linspace(0, 20, 100)
h0 = np.array([5, 10, 5, 10, 15, 0])

y1 = odeint(tank, h0, t, (tau, gamma, beta))


u8_s1 = np.vectorize(xi8)
vu8_s1 = u8_s1(t)

u4_s1 = np.vectorize(xi4)
vu4_s1 = u4_s1(t)


ax1 = plt.subplot(3, 2, 1)
ax1.plot(t,vu4_s1,'b--')
plt.xlabel('time (day)')
plt.title('Internal Cue' + '(' r'$\xi_4$' + ")")
ax1.set_ylim((-1, 10))

ax1 = plt.subplot(3, 2, 2)
ax1.plot(t,vu8_s1,'b--')
plt.xlabel('time (day)')
plt.title('External Cue' + '(' r'$\xi_8$' + ")")
ax1.set_ylim((0, 10))


ax1 = plt.subplot(3, 2, 3)
ax1.plot(t,y1[:,2],'b--')
plt.xlabel('time (day)')
plt.title('self-efficacy' + '(' r'$\eta_3$' + ")")
ax1.set_ylim((-1, 100))


ax1 = plt.subplot(3, 2, 4)
ax1.plot(t,y1[:,5],'b--')
plt.xlabel('time (day)')
plt.title('Cue to Action' + '(' r'$\eta_6$' + ")")

ax1 = plt.subplot(3, 2, 5)
ax1.plot(t,y1[:,1],'b--')
plt.xlabel('time (day)')
plt.title('Outcome Expectancy' + '(' r'$\eta_2$' + ")")
ax1.set_ylim((-1, 100))


ax1 = plt.subplot(3, 2, 6)
ax1.plot(t,y1[:,3],'b--', label = 'scenario1')
plt.axhline(y=50, linestyle = '--', dashes=(2,2), color = 'g')
plt.xlabel('time (day)')
plt.title('Behavior' + '(' r'$\eta_4$' + ")")
ax1.set_ylim((0, 100))

plt.tight_layout(pad=1, w_pad=0.5, h_pad=0.8)
plt.legend(loc=9, bbox_to_anchor=(-0.08, -0.2), ncol=2, prop={'size':11})

plt.show()


####no delay#######
def tank(h, t, tau, gamma, beta):
    dh1dt = (gamma[0][0] * xi1(t) + beta[0][3]*h[3]-h[0] + zeta1(t)) / tau[0]
    dh2dt = (gamma[1][1] * xi2(t) + beta[1][0]*h[0] + beta[1][4] * h[4] - h[1] + zeta2(t)) / tau[1]
    dh3dt = (gamma[2][1] * xi2(t) + gamma[2][2] * xi3(t) - gamma[2][4] * xi5(t) +
            gamma[2][5]*xi6(t) + beta[2][0] * h[0]+
            beta[2][3]*h[3] - h[2] + zeta3(t))/tau[2]
    
    dh4dt = (beta[3][1]*h[1] + beta[3][2]*h[2] + beta[3][5] * h[5] +
            beta[3][4] * h[4] - h[3] + zeta4(t))/tau[3]
    
    dh5dt = (gamma[4][6] * xi7(t) + beta[4][3]*h[3] - h[4] + zeta5(t)) / tau[4]
    dh6dt = (gamma[5][3] * xi4(t) + gamma[5][7]*xi8(t) - h[5] + zeta6(t)) / tau[5]
    
    dhdt = [dh1dt, dh2dt, dh3dt, dh4dt, dh5dt, dh6dt]
    
    return dhdt

#####parameters######

####time constants#######
def define_tau(num):
    tau = np.zeros(num)
    return tau

#####input/output resistance######
def define_matrix(num):
    gamma = np.zeros((num,num))
    return gamma

####input########
def xi1(t):
    return 0
def xi2(t):
    return 10
def xi3(t):
    return 10
def xi4(t):
    if  (t >= 14 and t <=15) or (t>=16 and t<=17) or (t >=18 and t <=19):
        return 5
    else:
        return 0
def xi5(t):
    return 2
def xi6(t):
    return 0
def xi7(t):
    return 0
def xi8(t):
    
    if t < 2 or t > 12 or (t >6 and t <8):
        return 0
    else:
        return 5

####noise######
def zeta1(t):
    return 0.0
def zeta2(t):
    return 0.0
def zeta3(t):
    return 0.0
def zeta4(t):
    return 0.0
def zeta5(t):
    return 0.0
def zeta6(t):
    return 0.0


tau = define_tau(6)
tau[0:3] = 1
tau[3] = 2
tau[4] = 1
tau[5] = 3

gamma = define_matrix(8)
gamma[0][0] = 3
gamma[1][1] = 1
gamma[2][1] = 2
gamma[2][2] = 1
gamma[2][4] = 1
gamma[2][5] = 1
gamma[4][6] = 2
gamma[5][3] = 15
gamma[5][7] = 15

beta = define_matrix(6)
beta[1][0] = 0.3
beta[2][0] = 0.5
beta[3][1] = 0.3
beta[3][2] = 0.8
beta[4][3] = 0.3
beta[2][3] = 0.2
beta[1][4] = 0.3
beta[0][3] = 0.23
beta[3][5] = 0.44
beta[3][4] = 0.1

t = np.linspace(0, 20, 100)
h0 = np.array([10, 20, 50, 50, 25, 0])

y2 = odeint(tank, h0, t, (tau, gamma, beta))


u8_s2 = np.vectorize(xi8)
vu8_s2 = u8_s2(t)


u4_s2 = np.vectorize(xi4)
vu4_s2 = u4_s2(t)


##############plot figure##########
ax1 = plt.subplot(3, 2, 1)
ax1.plot(t,vu4_s2,'r--')
plt.xlabel('time (day)')
plt.title('Internal Cue' + '(' r'$\xi_4$' + ")")
ax1.set_ylim((0, 10))

ax1 = plt.subplot(3, 2, 2)
ax1.plot(t,vu8_s2,'r--')
plt.xlabel('time (day)')
plt.title('External Cue' + '(' r'$\xi_8$' + ")")
ax1.set_ylim((0, 10))


ax1 = plt.subplot(3, 2, 3)
ax1.plot(t,y2[:,2],'r--')
plt.xlabel('time (day)')
plt.title('self-efficacy' + '(' r'$\eta_3$' + ")")
ax1.set_ylim((0, 100))


ax1 = plt.subplot(3, 2, 4)
ax1.plot(t,y2[:,5],'r--')
plt.xlabel('time (day)')
plt.title('Cue to Action' + '(' r'$\eta_6$' + ")")

ax1 = plt.subplot(3, 2, 5)
ax1.plot(t,y2[:,1],'r--')
plt.xlabel('time (day)')
plt.title('Outcome Expectancy' + '(' r'$\eta_2$' + ")")
ax1.set_ylim((0, 100))


ax1 = plt.subplot(3, 2, 6)
ax1.plot(t,y2[:,3],'r--', label = 'scenario2')
plt.axhline(y=50, linestyle = '--', dashes=(2,2), color = 'g')
plt.xlabel('time (day)')
plt.title('Behavior' + '(' r'$\eta_4$' + ")")
ax1.set_ylim((0, 100))

plt.tight_layout(pad=1, w_pad=0.5, h_pad=0.8)
plt.legend(loc=9, bbox_to_anchor=(-0.08, -0.2), ncol=2, prop={'size':11})
plt.show()



ax1 = plt.subplot(3, 2, 1)
ax1.plot(t,vu4_s1,'b-')
ax1.plot(t,vu4_s2,'r--')
plt.xlabel('time (day)')
plt.title('Internal Cue' + '(' r'$\xi_4$' + ")")
ax1.set_ylim((0, 10))


ax1 = plt.subplot(3, 2, 2)
ax1.plot(t,vu8_s1,'b-')
ax1.plot(t,vu8_s2,'r--')
plt.xlabel('time (day)')
plt.title('External Cue' + '(' r'$\xi_8$' + ")")
ax1.set_ylim((0, 10))


ax1 = plt.subplot(3, 2, 3)
ax1.plot(t,y1[:,2],'b-')
ax1.plot(t,y2[:,2],'r--')
plt.xlabel('time (day)')
plt.title('self-efficacy' + '(' r'$\eta_3$' + ")")
ax1.set_ylim((0, 100))


ax1 = plt.subplot(3, 2, 4)
ax1.plot(t,y1[:,5],'b-')
ax1.plot(t,y2[:,5],'r--')
plt.xlabel('time (day)')
plt.title('Cue to Action' + '(' r'$\eta_6$' + ")")

ax1 = plt.subplot(3, 2, 5)
ax1.plot(t,y1[:,1],'b-')
ax1.plot(t,y2[:,1],'r--')
plt.xlabel('time (day)')
plt.title('Outcome Expectancy' + '(' r'$\eta_2$' + ")")
ax1.set_ylim((0, 100))


ax1 = plt.subplot(3, 2, 6)
ax1.plot(t,y1[:,3],'b-', label = 'scenario1')
ax1.plot(t,y2[:,3],'r--', label = 'scenario2')
plt.axhline(y=50, linestyle = '--', dashes=(2,2), color = 'g')
plt.xlabel('time (day)')
plt.title('Behavior' + '(' r'$\eta_4$' + ")")
ax1.set_ylim((0, 100))
plt.legend(loc=9, bbox_to_anchor=(-0.08, -0.2), ncol=2, prop={'size':11})
plt.tight_layout(pad=1, w_pad=0.5, h_pad=0.8)
plt.show()

