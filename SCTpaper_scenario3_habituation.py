import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint


####no delay#######
def tank(h, t, tau, gamma, beta, with_habituation):
    dh1dt = (gamma[0][0] * xi1(t) + beta[0][3]*h[3]-h[0] + zeta1(t)) / tau[0]
    dh2dt = (gamma[1][1] * xi2(t) + beta[1][0]*h[0] + beta[1][4] * h[4] - h[1] + zeta2(t)) / tau[1]
    dh3dt = (gamma[2][1] * xi2(t) + gamma[2][2] * xi3(t) - gamma[2][4] * xi5(t) +
            gamma[2][5]*xi6(t) + beta[2][0] * h[0]+
            beta[2][3]*h[3] - h[2] + zeta3(t))/tau[2]
    
    dh4dt = (beta[3][1]*h[1] + beta[3][2]*h[2] + beta35(h[5], with_habituation) * h[5] +
            beta[3][4] * h[4] - h[3] + zeta4(t))/tau[3]
    
    dh5dt = (gamma[4][6] * xi7(t) + beta[4][3]*h[3] - h[4] + zeta5(t)) / tau[4]
    dh6dt = (gamma[5][3] * xi4(t) + gamma[5][7]*xi8(t) - h[5] + zeta6(t)) / tau[5]
    
    dhdt = [dh1dt, dh2dt, dh3dt, dh4dt, dh5dt, dh6dt]
    
    return dhdt

#####parameters######

####time constants#######
def define_tau(num):
    tau = np.zeros(num)
    return tau

#####input/output resistance######
def define_matrix(num):
    gamma = np.zeros((num,num))
    return gamma

####input########
def xi1(t):
    return 0
def xi2(t):
    return 10
def xi3(t):
    return 10
def xi4(t):
    return 0
def xi5(t):
    return 2
def xi6(t):
    return 0
def xi7(t):
    return 0
def xi8(t):
    if t >= 1 and t <= 14:
        return 7
    else:
        return 0

####noise######
def zeta1(t):
    return 0.0
def zeta2(t):
    return 0.0
def zeta3(t):
    return 0.0
def zeta4(t):
    return 0.0
def zeta5(t):
    return 0.0
def zeta6(t):
    return 0.0


def beta35(h, with_habituation):

    beta_35 = 0.44
  
    if with_habituation == 1:

        if h > 98:
            beta_35 = 0
        elif h > 92 and h <= 95:
            beta_35 = 0.4
    
    return beta_35


tau = define_tau(6)
tau[0:3] = 1
tau[3] = 2
tau[4] = 1
tau[5] = 3

gamma = define_matrix(8)
gamma[0][0] = 3
gamma[1][1] = 1
gamma[2][1] = 2
gamma[2][2] = 1
gamma[2][4] = 1
gamma[2][5] = 1
gamma[4][6] = 2
gamma[5][3] = 15
gamma[5][7] = 15

beta = define_matrix(6)
beta[1][0] = 0.3
beta[2][0] = 0.5
beta[3][1] = 0.3
beta[3][2] = 0.8
beta[4][3] = 0.3
beta[2][3] = 0.2
beta[1][4] = 0.3
beta[0][3] = 0.23
beta[3][5] = 0.44
beta[3][4] = 0.1


t = np.linspace(0, 20, 100)
h0 = np.array([20, 25, 50, 50, 25, 0])

with_habituation = 1
y3_with_habituation = odeint(tank, h0, t, (tau, gamma, beta, with_habituation))

with_habituation = 0
y3_without_habituation = odeint(tank, h0, t, (tau, gamma, beta, with_habituation))

u8_s3 = np.vectorize(xi8)
vu8_s3 = u8_s3(t)

###########plot figures##############
ax1 = plt.subplot(3, 1, 1)
ax1.plot(t,vu8_s3,'b-')
ax1.plot(t,vu8_s3,'r--')
plt.xlabel('time (day)')
plt.title('External Cue' + '(' r'$\xi_8$' + ")")
ax1.set_ylim((0, 10))

ax1 = plt.subplot(3, 1, 2)
ax1.plot(t,y3_with_habituation[:,5],'r--')
ax1.plot(t,y3_without_habituation[:,5],'b-')
plt.xlabel('time (day)')
plt.title('Cue to Action' + '(' r'$\eta_6$' + ")")
ax1.set_ylim((0, 100))

ax1 = plt.subplot(3, 1, 3)
ax1.plot(t,y3_with_habituation[:,3],'r--', label = 'with habituation')
ax1.plot(t,y3_without_habituation[:,3],'b-', label = 'without habituation')
plt.axhline(y=50, linestyle = '--', dashes=(2,2), color = 'g')
plt.xlabel('time (day)')
plt.title('Behavior' + '(' r'$\eta_4$' + ")")
ax1.set_ylim((0, 100))

plt.tight_layout(pad=1, w_pad=0.5, h_pad=0.8)
plt.legend(loc=9, bbox_to_anchor=(0.79, -0.2), ncol=2, prop={'size':10}, columnspacing = 0.6)
plt.show()


